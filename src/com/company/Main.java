package com.company;

import java.util.Random;

/**
 * ДЗ 1, Задача 2
 * по итогам review: вынес выполнение всего кода в метод main, убрал метод method
 */

public class Main {
    public static void main(String[] args) {
        // write your code here
        final Random random = new Random();
        final int n = 20;
        int k;
        double q;

        for (int i = 0; i < n; i++) {
            k = random.nextInt(100) - 50; // генерируем случайное число
            try {
                if (k < 0) {
                    throw new Exception("k - отрицательное(" + k + ")"); // если отрицательное - бросаем исключение
                }

                q = Math.sqrt(k); // находим корень
                if (Math.pow((int) q, 2) == k) { // сравниваем квадрат целой части корня с исходным
                    System.out.println("k = " + k);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
